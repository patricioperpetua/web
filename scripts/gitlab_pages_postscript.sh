#!/usr/bin/env bash

# Web Page of BASH best practices https://kvz.io/blog/2013/11/21/bash-best-practices/
#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

aws --version  # Verify AWS CLI is installed

echo "Configuring AWS credentials..."
aws configure set aws_access_key_id $NG_APP_AWS_S3_ACCESS_KEY_ID
aws configure set aws_secret_access_key $NG_APP_AWS_S3_SECRET_ACCESS_KEY
aws configure set default.region $NG_APP_AWS_S3_REGION

AMAZON_S3_BUCKET="patricioperpetua.com"
AMAZON_S3_FOLDER="/assets/resume/latest/"

LOCAL_FOLDER="public/assets/resume/latest/"

# Replace the base href with an empty string to avoid the 404 error when the page is deployed to GitLab Pages
# https://stackoverflow.com/questions/49946570/gitlab-pages-404-error-on-reload
sed -i.bak 's|<base href=\"[^\"]*\">|<base href=\"\">|' public/index.html

mkdir -p ${LOCAL_FOLDER}

# Copy the latest resume to the local folder
aws s3 cp s3://${AMAZON_S3_BUCKET}${AMAZON_S3_FOLDER} ${LOCAL_FOLDER} --recursive --exclude "*" --include "*.*"
